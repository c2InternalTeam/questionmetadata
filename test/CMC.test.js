let React = require('react');
let ReactDOM = require('react-dom');
let TestUtils = require('react-addons-test-utils'); //I like using the Test Utils, but you can just use the DOM API instead.
let ChapterOrSectionMetadata = require('../js/components/ChapterOrQuestionMetadata'); //my root-test lives in components/__tests__/, so this is how I require in my components.
let chai = require('chai');
let expect = chai.expect;

describe('ChapterOrSectionMetadata testcases', function () {
  before('before creating ChapterOrSectionMetadata', function() {
    this.ChapterOrSectionMetadataComp = TestUtils.renderIntoDocument(<ChapterOrSectionMetadata />);
    //this.input = TestUtils.findRenderedDOMComponentWithTag(this.ChapterOrSectionMetadataComp, 'ChapterOrSectionMetadata');
  });

 

  it('renders a ChapterOrSectionMetadata with default ChapterOrSectionNumber as empty', function () {
    expect(this.ChapterOrSectionMetadataComp).to.exist;
    expect(this.ChapterOrSectionMetadataComp.state.ChapterOrSectionNumber).to.equal("");
  });

  it('renders a ChapterOrSectionMetadata with default ChapterOrSectionTitle as empty', function () {
    expect(this.ChapterOrSectionMetadataComp).to.exist;
    expect(this.ChapterOrSectionMetadataComp.state.ChapterOrSectionTitle).to.equal("");
  });

  it("renders a list in a box with proper CSS classes", function() {
    let renderedDOM = ReactDOM.findDOMNode(this.ChapterOrSectionMetadataComp);
    expect(renderedDOM.tagName).to.equal("DIV");
    expect(renderedDOM.classList.length).to.equal(0);
    expect(renderedDOM.children.length).to.equal(3);
  });

  /* it('renders a ChapterOrSectionMetadata with default ChapterOrSectionTitle as empty', function () {
    expect(this.ChapterOrSectionMetadata).to.exist;
    expect(this.ChapterOrSectionMetadata.ChapterOrSectionTitle).to.equal("");
  });

  it('allows changing components ChapterOrSectionNumber', function () {
    TestUtils.Simulate.change(this.ChapterOrSectionMetadata, {target: {ChapterOrSectionNumber: 'Hello, world'}});
    expect(this.ChapterOrSectionMetadataComp.state.ChapterOrSectionNumber).to.equal("Hello, world");
  });

  it('allows changing components ChapterOrSectionTitle', function () {
    TestUtils.Simulate.change(this.ChapterOrSectionMetadata, {target: {ChapterOrSectionTitle: 'Hello, world'}});
    expect(this.ChapterOrSectionMetadataComp.state.ChapterOrSectionTitle).to.equal("Hello, world");
  });

  it('changes ChapterOrSectionNumber after setting state', function () {
    let ChapterOrSectionMetadataComp1 = TestUtils.renderIntoDocument(<ChapterOrSectionMetadata/>);
    ChapterOrSectionMetadataComp1.setState({ ChapterOrSectionNumber: "New state value" });
    let ChapterOrSectionMetadata1 = TestUtils.findRenderedDOMComponentWithTag(ChapterOrSectionMetadataComp1, 'ChapterOrSectionMetadata');
    expect(ReactDOM.findDOMNode(ChapterOrSectionMetadata1).getAttribute("ChapterOrSectionNumber")).not.to.equal("New state value");
  });


it('changes ChapterOrSectionTitle after setting state', function () {
    let ChapterOrSectionMetadataComp1 = TestUtils.renderIntoDocument(<ChapterOrSectionMetadata/>);
    ChapterOrSectionMetadataComp1.setState({ ChapterOrSectionTitle: "New state value" });
    let ChapterOrSectionMetadata1 = TestUtils.findRenderedDOMComponentWithTag(ChapterOrSectionMetadataComp1, 'ChapterOrSectionMetadata');
    expect(ReactDOM.findDOMNode(ChapterOrSectionMetadata1).getAttribute("ChapterOrSectionTitle")).not.to.equal("New state value");
  });


  it('passing "ChapterOrSectionNumber" property updates state and DOM element', function () {
    let ChapterOrSectionMetadataComp1 = TestUtils.renderIntoDocument(<ChapterOrSectionMetadata ChapterOrSectionNumber="sample"/>);
    expect(ChapterOrSectionMetadataComp1.state.ChapterOrSectionNumber).to.equal("sample");
    console.log("000",ChapterOrSectionMetadataComp1.state);
    let ChapterOrSectionMetadata = TestUtils.findRenderedDOMComponentWithTag(ChapterOrSectionMetadataComp1, 'ChapterOrSectionMetadata');
    console.log("111",ReactDOM.findDOMNode(ChapterOrSectionMetadata));
    expect(ReactDOM.findDOMNode(ChapterOrSectionMetadata).getAttribute("ChapterOrSectionNumber")).to.equal('sample');
  });


  it('passing "ChapterOrSectionTitle" property updates state and DOM element', function () {
    let ChapterOrSectionMetadataComp1 = TestUtils.renderIntoDocument(<ChapterOrSectionMetadata ChapterOrSectionTitle="sample"/>);
    expect(ChapterOrSectionMetadataComp1.state.ChapterOrSectionTitle).to.equal("sample");
    console.log("000",ChapterOrSectionMetadataComp1.state);
    let ChapterOrSectionMetadata = TestUtils.findRenderedDOMComponentWithTag(ChapterOrSectionMetadataComp1, 'ChapterOrSectionMetadata');
    console.log("111",ReactDOM.findDOMNode(ChapterOrSectionMetadata));
    expect(ReactDOM.findDOMNode(ChapterOrSectionMetadata).getAttribute("ChapterOrSectionTitle")).to.equal('sample');
  });*/


 });
