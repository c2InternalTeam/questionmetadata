let React = require('react');
let ReactDOM = require('react-dom');
let TestUtils = require('react-addons-test-utils'); //I like using the Test Utils, but you can just use the DOM API instead.
let TextBox = require('../js/components/common/SelectBox'); //my root-test lives in components/__tests__/, so this is how I require in my components.
let chai = require('chai');
let expect = chai.expect;

describe('SelectBox testcases', function () {
  before('before creating selectbox', function() {
    this.SelectBoxComp = TestUtils.renderIntoDocument(<SelectBox />);
    this.input = TestUtils.findRenderedDOMComponentWithTag(this.SelectBoxComp, 'input');
  });

  /*it('Renderes Input tag with "pe-input" class', function() { 
    let renderedDOM = ReactDOM.findDOMNode(this.TextBoxComp);
    expect(renderedDOM.tagName).to.equal("INPUT");
    expect(renderedDOM.classList.length).to.equal(1);
    expect(renderedDOM.classList[0]).to.equal("pe-input");
  });
*/
  it('renders a input with default value as empty', function () {
    expect(this.input).to.exist;
    expect(this.input.value).to.equal("");
  });

  it('passing "required" property updates state and DOM element', function () {
    let SelectBoxComp = TestUtils.renderIntoDocument(<SelectBox required={true}/>);
    expect(SelectBoxComp.state.required).to.equal(true);
    let input = TestUtils.findRenderedDOMComponentWithTag(SelectBoxComp, 'input');
    console.log("---->",input);
    expect(ReactDOM.findDOMNode(input).getAttribute("required")).to.equal(true);
  });

   it('passing "disabled" property updates state and DOM element', function () {
    let SelectBoxComp = TestUtils.renderIntoDocument(<SelectBox disabled={true} />);
    expect(SelectBoxComp.state.disabled).to.equal(true);
    let input5 = TestUtils.findRenderedDOMComponentWithTag(SelectBoxComp, 'input');
    console.log("---->",input5);
    expect(ReactDOM.findDOMNode(input5).getAttribute("disabled")).to.equal(true);
  });


  it("has some value selected by default", function() {
    let selected = this.selectedDishDOM();

    expect(selected).toBeDefined();
    expect(selected.textContent).toEqual("Select a content type");
    expect(selected.value).toEqual("Select a content type");
  });

  it("has all restraurant's best dishes", function() {
    let dishesPresented = this.availableDishes();

    expect(dishesPresented.length).toEqual(3);
    expect(dishesPresented).toContain({ name: "Fried Bacon", value: "fried-bacon" });
    expect(dishesPresented).toContain({ name: "Fish & Chips", value: "fish-and-chips" });
    expect(dishesPresented).toContain({ name: "Potato Salad", value: "potato-salad" });
  });




  it('allows changing component value', function () {
    TestUtils.Simulate.change(this.input, {target: {value: 'Hello, world'}});
    expect(this.SelectBoxComp.state.value).to.equal("Hello, world");
  });

  it('changes value after setting state', function () {
    let SelectBoxCompSelectBoxComp = TestUtils.renderIntoDocument(<SelectBox/>);
    SelectBoxComp.setState({ value: "New state value" });
    let input1 = TestUtils.findRenderedDOMComponentWithTag(SelectBoxComp, 'input');
    expect(ReactDOM.findDOMNode(input1).getAttribute("value")).not.to.equal("New state value");
  });

  it('passing "value" property updates state and DOM element', function () {
    let SelectBoxComp = TestUtils.renderIntoDocument(<SelectBox value="sample"/>);
    expect(SelectBoxComp.state.value).to.equal("sample");
    console.log("000",SelectBoxComp.state);
    let input = TestUtils.findRenderedDOMComponentWithTag(SelectBoxComp, 'input');
    console.log("111",ReactDOM.findDOMNode(input));
    expect(ReactDOM.findDOMNode(input).getAttribute("value")).to.equal('sample');
  });

});





