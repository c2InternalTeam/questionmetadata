let buffer = {
            'libConfig' : {'locale': 'en_US',
                   'environment': 'dev',
                   'link': 'google.com1',

                   'headers' : {'Content-Type'   : 'application/json',
                                'Accept'         : 'application/ld+json',
                                'X-Roles'        : 'roleX,roleY,roleC',
                                'Authorization'  : 'Basic c3RyYXdiZXJyeToqbnJSUEc0akA1b1JCUnVDMkckITh4IzVqSFA='
                               },
                   'database'       : '?db=qa6',
                   'server'         : 'https://uat.pearsonmeta.io',
                   'port'           : '8080'
                  },          // Includes host app supplied init values for lib + Should have all Taxonomies
            'patConfig' : {arg : '00001', link : 'button.com1', selector : '#patternHolder1'},          // Includes host app supplied init values for pattern
            'req'       : 'ProductMetadata',             // ProductMetadata / AssessmentMetadata / QuestionMetadata / AssetMetadata
            'action'    : 'Create',                      // Create / Read One / Update / Delete / Search / Read All
            'data' : {
                'uuid':'',                                // Only required for actioon = Update/Delete/Read
                'status' : '',                           // Not sure if this is needed
                'filename' : '',                         // This is prob. only for Asset
                'discipline' : 'Mathematics',            // Taxonomy value
                'difficultyLevel' : 'Easy',              // Taxonomy value
                'trigger' : '',
                'knowledgeLevel' : 'Grade 10',
                'hasAlignment' : '',                     // Taxonomy value
                'keywords' : ['key1', 'key2', 'key3'],
                'name': 'Some Product Name',
                'timeRequired' : {
                    'hh':'01',
                    'mm':'20',
                    'ss':'30'
                },
                'description' : 'Some description for Product with decipline Math and knowledge level of Grade 10',
                'audience' : 'Parents',                  // Taxonomy value
                'searchterms' : ''
            }
        }
 
let bufferGet = {
            'libConfig' : {'locale': 'en_US',
                   'environment': 'dev',
                   'link': 'google.com1',

                   'headers' : {'Content-Type'   : 'application/json',
                                'Accept'         : 'application/ld+json',
                                'X-Roles'        : 'roleX,roleY,roleC',
                                'Authorization'  : 'Basic c3RyYXdiZXJyeToqbnJSUEc0akA1b1JCUnVDMkckITh4IzVqSFA='
                               },
                   'database'       : '?db=qa6',
                   'server'         : 'https://uat.pearsonmeta.io',
                   'port'           : '8080'
                  },          // Includes host app supplied init values for lib + Should have all Taxonomies
            'patConfig' : {arg : '00001', link : 'button.com1', selector : '#patternHolder1'},          // Includes host app supplied init values for pattern
            'req'       : 'ProductMetadata',             // ProductMetadata / AssessmentMetadata / QuestionMetadata / AssetMetadata
            'action'    : 'Read One',                      // Create / Read One / Update / Delete / Search / Read All

            'data' : {
                'uuid':''                                 // Only required for actioon = Update/Delete/Read
            }
}

var MDSConstants = {
	BUFFER : buffer,
	BUFFER_GET : bufferGet
}

module.exports = MDSConstants;
