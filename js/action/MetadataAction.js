/*
 * Copyright (c) Pearson, Inc.
 * All rights reserved.
 *.
 *
 * LinkMetaDataActions
 */
import MetaDataApi from '../api/MetadataApi'
import * as types from '../constants/MVMConstants'
import MetaDataService from '../common/util/metadataService';
import MDSConstants from '../constants/MDSConstants'

/*  export function  fetchMetaData (){
    return dispatch => {
      MetaDataApi.get_QMD_Data().then(function(data){ 
        dispatch({
          type: types.METADATA_GET,
          QMD_Data : JSON.parse(data.text)
        })
      })
  }
}

  export function  saveMetaData (values){
    return dispatch => {
      MetaDataApi.save_QMD_Data(values).then(function(data){ 
        dispatch({
          type: types.SAVE_METADATA,
          QMD_Data : JSON.parse(data.text),values
        })
      })
  }
}*/

export function  fetchMetaData (metadata){debugger;
    return (dispatch, getState) => {
      let state = getState();
      let bufferGet = MDSConstants.BUFFER_GET;
      let metaDataState = state.Metadatareducers;
      //make service call get the metadata based on uuid
      //metaDataState.uuid = "a92df0fa-d68d-41fe-9594-a0fdd5cf43e3";
      //metaDataState.uuid = "872c79ad-c284-4bac-bf3a-a12048674f41";
      //metaDataState.uuid = "c5ef8548-fb93-4710-b82c-334fe5a04f21";
      //metaDataState.uuid = "b2e1113d-410e-4a24-86f9-35cf69f65732"; //whitman
      // metaDataState.uuid = "185bddcd-439d-4c45-a8c9-54e6b6ba7f63";
    //metaDataState.uuid = "f2829d04-4df7-4ca7-858f-e865244594ff";
    //metaDataState.uuid = "6e143c65-6abd-4af4-80fd-e1a657c8132f"; //whitman dickson english
    //metaDataState.uuid = "60cebec2-19cc-4f40-9903-94bc4562f550"; //for combo box
      if(metaDataState && metaDataState.uuid && metaDataState.uuid!==''){
          bufferGet.data.uuid = metaDataState.uuid;
          let promise = MetaDataService.send(bufferGet);
          promise.then(function (replyGet) {debugger;
             var itemArr = [];
            if(Array.isArray(replyGet.keywords)){
                for(var i=0; i<replyGet.keywords.length;i++){
                  itemArr.push({"id": i,"name" : replyGet.keywords[i]});
                }
            }else if(typeof(replyGet.keywords) === 'string'){
              itemArr.push({"id": 0,"name" : replyGet.keywords});
            }
            replyGet.keywords = itemArr;
              for (var key in replyGet) {
                  metadata[key] = replyGet[key];
              }
              console.log('Button:onClick - create send successful response received %o', replyGet);
              dispatch({
                type: types.SET_UUID,
                QMD_Data : metadata
              })
            },function (replyGet) {
              console.log('Button:onClick - create send failed error message received %o', replyGet);
          });
        

      }else{
        dispatch({
          type: types.METADATA_GET,
          QMD_Data : metadata
        })
      } 
    }
}

export function  fetchMetaDataTaxonomy (){
    return dispatch => {
      MetaDataApi.get_QMD_Data().then(function(data){ 
        dispatch(fetchMetaData(JSON.parse(data.text)))
      })
  }
}

export function  saveMetaData (values){debugger;
    return dispatch => {
      let buffer = MDSConstants.BUFFER;
      //buffer.data = values;
        /*for (var name in values.keywords) {
                  console.log('key : '+name);

              }*/
      var itemArr = [];
     for(var i=0; i<values.keywords.length;i++){
        console.log('name : '+values.keywords[i].name);
        itemArr.push(values.keywords[i].name);
     }
     console.log('itemArr : '+itemArr);
     values.keywords = itemArr;
     // values.keywords = "";
     var timeReq = {};
     /*console.log('values.hours Store : '+values.hours);
     console.log('values.mins Store : '+values.mins);
     console.log('values.secs Store : '+values.secs);*/
     //timeReq.push(values.hours);
     //timeReq.push({"id": 'hh',"value" : values.hours});
     // timeReq.push({'hh' : values.hours});
    //timeReq.push(values.mins);
    //timeReq.push({"id": 'mm',"value": values.mins});
    // timeReq.push({'mm' : values.mins});
     //timeReq.push(values.secs);
     //timeReq.push({"id": 'ss',"value" : values.secs});
     //timeReq.push({'ss' : values.secs});
     /*timeReq.push({'hh' : values.hours,'mm' : values.mins,'ss' : values.secs});
     console.log('TimeReq Store : '+timeReq);*/
     timeReq.hh = values.hours;
     timeReq.mm = values.mins;
     timeReq.ss = values.secs;
     values.timeRequired = timeReq;
     buffer.data = values;
      let promise = MetaDataService.send(buffer);
        promise.then(function (replyCreate) {
            values.uudi = replyCreate.uuid;
            console.log('Button:onClick - create send successful response received %o', replyCreate);
            dispatch({
              type: types.SET_UUID,
              QMD_Data : values
            })
          },function (replyCreate) {
            console.log('Button:onClick - create send failed error message received %o', replyCreate);
        });
     
  }
}

export function populateAutoComplete(text) {
    return dispatch => {
      MetaDataApi.get_AutoComplete_Data(text).then(function(data){ 
        dispatch({
          type: types.AUTO_COMPLETE,
          data: JSON.parse(data.text),
          text
        });
      });
  }
}
