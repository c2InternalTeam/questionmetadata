import { combineReducers } from 'redux'
import Metadatareducers from './Metadatareducers'
import {reducer as formReducer} from 'redux-form';
import autoComplete from './autoCompleteReducer'

const rootMetaData = combineReducers({
	Metadatareducers,
	autoComplete, 
	form: formReducer 
});

export default rootMetaData
