import React from 'react'
import MVMContainer from '../container/MetadataContainer'

const App = () => (
  <div>
    <MVMContainer/>	  
  </div>
)

export default App
