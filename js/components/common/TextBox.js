/**
 * Copyright
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

/**
 * This component operates as a "Controller-View".  It listens for changes in
 * the TodoStore and passes the new data to its children.
 */
import React from 'react';

class TextBox extends React.Component{

constructor(props) {
    super(props);
    this.displayName = 'TextBox';
 }

static propTypes= {
    id:React.PropTypes.string,
    value: React.PropTypes.object,
    placeholder: React.PropTypes.string,
    disabled: React.PropTypes.bool,
    required: React.PropTypes.bool,
    maxLength:React.PropTypes.string,
    autofocus: React.PropTypes.bool,
    
}
static defaultProps ={
      id:'',
      value: '',
      placeholder:'',
      disabled:false,
      required:false,
      maxLength:'30',
      autofocus: false,
}

render() {
          
        return (
            <input 
            id={this.props.id}
              ref="input" type="text"
               required={this.props.required} 
               maxLength={this.props.maxLength}
                value ={this.props.value} {...this.props.value} 
                placeholder={this.props.placeholder} 
                readOnly={this.props.readOnly}
                 disabled={this.props.disabled}
            />
        )
    }

};

module.exports = TextBox;
