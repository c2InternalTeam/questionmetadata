import React from 'react';
import NumericInput from 'react-numeric-input';

class HoursComponent extends React.Component {

constructor(props) {
    super(props);
    this.displayName = 'HoursComponent';
     this.state={
            hours:this.props.hours
        };
 }
static propTypes= {
    id:React.PropTypes.string,
    hours: React.PropTypes.object    
}
 static defaultProps ={
          id:'',
          hours:''
  }

	myFormat(num) {
		if(num >=10){
			return num;
		}else{
    		return 0 + num;
    	}
	}

  hoursOnchange(e){
    this.state.hours = this.myFormat(e);
  }

   render() {
      return (
         <div>
            <label>Hours</label><NumericInput min={0} max={24} size={1} format={this.myFormat} name="hh" value={this.state.hours} {...this.props.hours} />
         </div>
      );
   }
}

module.exports = HoursComponent;