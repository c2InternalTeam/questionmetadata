import React from 'react';
import NumericInput from 'react-numeric-input';

class MinutesComponent extends React.Component {

constructor(props) {
    super(props);
    this.displayName = 'MinutesComponent';
     this.state={
            mins:this.props.mins
        };
 }

 static propTypes= {
    id:React.PropTypes.string,
    mins: React.PropTypes.object    
}
 static defaultProps ={
          id:'',
          mins:''
  }

	myFormat(num) {
		if(num >=10){
			return num;
		}else{
    		return 0 + num;
    	}
	}

  minsOnchange(e){
    this.state.mins = this.myFormat(e);
  }

   render() {
      return (
         <div>
            <label>Minutes</label><NumericInput min={0} max={60} size={1} format={this.myFormat} name="mm" value={this.state.mins}{...this.props.mins}/>
         </div>
      );
   }
}

module.exports = MinutesComponent;