import React from 'react';
import NumericInput from 'react-numeric-input';

class App extends React.Component {

  constructor(props){
        super(props);
        this.state={
            hours:this.props.hours,
            mins : this.props.mins,
            secs:this.props.secs,
            timeReq: this.props.timeReq
        };
  }

  static defaultProps ={
          hours:'',
          mins: '',
          secs:'',
          timeReq: ''
    }

  myFormat(num) {
    if(num >=10){
      return num;
    }else{
        return 0 + num;
      }
  }

  hoursOnchange(e){
    console.log('e: '+e);
    this.state.hours = this.myFormat(e);
    console.log('this.state.hours: '+this.state.hours);
  }

  minsOnchange(e){
    console.log('e: '+e);
    this.state.mins = this.myFormat(e);
    console.log('this.state.mins: '+this.state.mins);
  }

  secsOnchange(e){
    console.log('e: '+e);
    this.state.secs = this.myFormat(e);
    console.log('this.state.secs: '+this.state.secs);
  }

   render() {
      return (
         <div>
            <label>Hours</label><NumericInput min={0} max={24} size={1} format={this.myFormat} name="hh" value={this.state.hours} onChange={this.hoursOnchange.bind(this)}/>&nbsp;&nbsp;
            <label>Minutes</label><NumericInput min={0} max={60} size={1} format={this.myFormat} name="mm" value={this.state.mins} onChange={this.minsOnchange.bind(this)}/>&nbsp;&nbsp;
            <label>Seconds</label><NumericInput min={0} max={60} size={1} format={this.myFormat} name="ss" value={this.state.secs} onChange={this.secsOnchange.bind(this)}/>&nbsp;&nbsp;
         </div>
      );
   }
}

export default App;